import os
from sqlalchemy import  create_engine
from sqlalchemy.orm.session import sessionmaker
from sqlalchemy.ext.declarative import declarative_base #ESTO VA A SERVIR PARA MANIPULAR TODAS LAS TABLAS EN MI BASE DE DATOS

sqlite_file_name = "../database.sqlite"
base_dir = os.path.dirname(os.path.realpath(__file__))


#URL DE MI BASE DE DATOS    
database_url = f"sqlite:///{os.path.join(base_dir, sqlite_file_name)}"

#REPRESENTA EL MOTOR DE LA BASE DE DATOS
engine = create_engine(database_url, echo=True)

#CREO UNA SESIÓN PARA CONTECTARME A LA BASE DE DATOS
Session = sessionmaker(bind=engine)

Base = declarative_base()       #BASE ES UNA INSTANCIA
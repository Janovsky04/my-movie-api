from config.database import Base
from sqlalchemy import Column, Integer, String, Float

class Movie(Base):      #ESTO VA A SER UNA ENTIDAD DE MI BASE DE DATOS
    
    __tablename__ = "movies"
    
    id = Column(Integer, primary_key=True)
    title = Column(String)
    overview = Column(String)
    year = Column(Integer)
    rating = Column(Float)
    category = Column(String)
